import { createAudioResource, joinVoiceChannel, createAudioPlayer, PlayerSubscription, VoiceConnection, AudioPlayer, } from '@discordjs/voice'
import { Client, CommandInteraction, GuildMember } from 'discord.js'
import *  as Config from "../config.json"
import ytdl from 'ytdl-core';
import { DatabaseInterface } from 'database';


export class VoiceChannels {
	client: Client;
	queue: String[];
	conn?: VoiceConnection;
	player: AudioPlayer;
	constructor(client: Client) {
		this.client = client;
		this.queue = [];
		this.player = createAudioPlayer({ behaviors: { maxMissedFrames: 69 } })
	}
	ppl_on_vc = (): Promise<{ dsc_id: string, vc_id: string }[]> => {
		return new Promise((res, err) => {
			this.client.guilds.fetch(Config.authorized_guild).then((aGilds) => {
				aGilds.members.fetch().then((members) => {
					let members_in_vc: { dsc_id: string, vc_id: string }[] = [];
					members.forEach((member) => {
						let voice = member.voice.channelId;
						if (voice != null) {
							members_in_vc.push({ dsc_id: member.id, vc_id: voice });
						}
					})
					res(members_in_vc);
				})
			})

		});

	}
	music = (interaction: CommandInteraction, DBApp: DatabaseInterface) => {
		if(!(interaction.member instanceof GuildMember)) 
			return interaction.editReply('An error has occured.')
			
		if (this.conn == undefined && interaction.member?.voice.channelId != null) {
			this.conn = joinVoiceChannel({
				channelId: interaction.member?.voice.channelId,
				guildId: interaction.member?.voice.guild.id,
				adapterCreator: interaction.member?.voice.guild.voiceAdapterCreator
			})
		}
		let content = interaction.options.getString('content')
		if(!content) return interaction.editReply('An error has occured.')
		
		this.to_queue(content)
		if (this.player.state.status == "idle") {
			this.from_queue()
		}
		this.player.on("stateChange", () => {
			if (this.player.state.status == "idle") {
				this.from_queue()
			}
		})

	}

	to_queue = (msg: string,) => {
		let links = msg.split(" ")
		if (links != null) {
			for (let link of links) {
				for (let site of Config.VoiceModuleConf.AllowedStites) {
					if (link.indexOf("https://" + site) == 0) {
						this.queue.push(link)
					}
				}
			}
		}

	}
	from_queue = () => {
		let src = this.queue.shift()
		if (src != undefined) {
			let stream = ytdl(String(src), { filter: 'audioonly' });
			let resource = createAudioResource(stream, {
				inlineVolume: true
			})
			this.conn?.subscribe(this.player)
			this.player.play(resource)
		}
	}
	kill = () => {
		this.queue = []
		this.player.stop()
	}
}

