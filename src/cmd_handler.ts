import { DatabaseInterface } from "database";
import { ApplicationCommandData, ApplicationCommandOptionData, Client, CommandInteraction } from "discord.js"
import StatsManager from "stats";
import { VoiceChannels } from "./voice";
import { commands } from "./main";
import * as Conf from "./../config.json";

interface Command {
    name: string
    description: string
    options: ApplicationCommandOptionData[]
    run: (interaction: CommandInteraction, DBApp: DatabaseInterface) => void
}

export default class CommandHandler {
    getCommands = (StatsApp: StatsManager, VcManager: VoiceChannels) => {
        const commands: Command[] = [
            {
                name: 'statsall',
                run: StatsApp.send_statsall,
                description: 'Display all stats.',
                options: []
            },
            {
                name: 'stats',
                run: StatsApp.send_stats,
                description: 'Display statsof a user.',
                options: [
                    {
                        type: 'MENTIONABLE',
                        name: 'user',
                        description: 'Mention the user you want to get stats for.',
                        required: true
                    }
                ]
            },
            {
                name: 'p',
                run: VcManager.music,
                description: 'Plays music I guess.',
                options: [
                    {
                        type: 'STRING',
                        name: 'content',
                        description: 'The thing to play.',
                        required: true
                    }
                ]
            },
            {
                name: 's',
                run: VcManager.from_queue,
                description: 'This does something.',
                options: []
            },
            {
                name: 'k',
                run: VcManager.kill,
                description: 'THIS KILLS.',
                options: []
            }
        ]

        return commands
    }
    getCommandsToRegister = () => {
        return commands.map(command => {
            const { name, description, options } = command
            const commandData: ApplicationCommandData = {
                name,
                description,
                options,
                defaultPermission: true
            }
            
            return commandData
        })
    }

    registerCommands = async (client: Client) => {
        const commandsToSet = this.getCommandsToRegister()    
    
        const testGuildId = Conf.commands_guild
        if(testGuildId) {
            const guild = await client.guilds.fetch(testGuildId)
            guild.commands.set(commandsToSet)
        } else {
            const botCommands = client.application?.commands
    
            if (!botCommands) return console.error('No bot commands.')
        
            botCommands.set(commandsToSet)
                .then(res => console.log(res)
                )
                .catch(err => console.error(err))
        }
    }

    handleCommand = (interaction: CommandInteraction, DBApp: DatabaseInterface) => {
        if(!interaction) return console.error('No interaction.')
        let { commandName } = interaction

        interaction.reply('Executing - please wait.')
            .then(() => {
                const command = commands.filter((cmd) => cmd.name === commandName)[0];

                if(!command) console.error('no command!');

                console.log(command);
                
                
                command.run(interaction, DBApp)
            })
    }
}
