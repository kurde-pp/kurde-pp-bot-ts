import { DatabaseInterface } from './database';
import { Client, Intents, Message, ThreadChannel } from 'discord.js';
import * as APITokens from './../token.json';
import * as Conf from "./../config.json";
import { TaskManager } from './tasks';
import { VoiceChannels } from "./voice";
import StatsManager from './stats';
import CommandHandler from './cmd_handler';


const client = new Client({
	intents: [Intents.FLAGS.GUILD_INTEGRATIONS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS, Intents.FLAGS.GUILD_MESSAGE_TYPING, Intents.FLAGS.DIRECT_MESSAGES, Intents.FLAGS.DIRECT_MESSAGE_TYPING, Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_VOICE_STATES]
});
const DBAapp = new DatabaseInterface();
const TaskMng = new TaskManager();
const VcManager = new VoiceChannels(client);
const StatsApp = new StatsManager();
const CmdHandler = new CommandHandler();

export const commands = CmdHandler.getCommands(StatsApp, VcManager)

let lesson_send = (time: number[]) => {
	console.log("event on: " + time[0] + ":" + time[1])

}

let lessons_event = () => {
	let now = new Date();
	const day_in_ms = 24 * 60 * 60 * 1000;
	Conf.LessonsConf.lessons_time.forEach((lesson_hour) => {
		let lesson_time = now;
		lesson_time.setHours(lesson_hour[0], lesson_hour[1]);
		lesson_time.setTime(lesson_time.getTime() - Conf.LessonsConf.time_offset * 60 * 1000);
		if (lesson_time.getTime() < now.getTime()) {
			lesson_time.setTime(lesson_time.getTime() + day_in_ms);
		}
		TaskMng.tasks.push({ timestamp: lesson_time.getTime(), repeat: day_in_ms, args: lesson_hour, callback: lesson_send });
	})

}



let load_events = () => {
	TaskMng.tasks.push({
		timestamp: minute_after_login, args: null, repeat: 60000, callback: () => {
			VcManager.ppl_on_vc().then((res) => {
				if (res.length > 0) {
					DBAapp.save_vc_info(res, new Date().getTime());
				}
				console.log(res);

			})
		}
	});
	lessons_event();

}


let minute_after_login: number;
client.once('ready', () => {
	CmdHandler.registerCommands(client);

	minute_after_login = new Date().getTime() + 60 * 1000;
	console.log(`Logged in as ${client.user?.tag}!`);
	load_events();
	DBAapp.get_lesson([8, 0, 2]).then((res) => {
		console.log(res)
	})
});

client.on('messageCreate', (msg: Message) => {
	if (msg.guildId != null) {
		if (Conf.authorized_guild == msg.guild?.id) {
			DBAapp.save_message(msg);
			
		}
	}
});

client.on('interactionCreate', (interaction) => {
    if(!interaction.isCommand()) return

	CmdHandler.handleCommand(interaction, DBAapp)
})


client.on('threadCreate', (thread: ThreadChannel) => {
	thread.join();
});

client.login(APITokens.discord);