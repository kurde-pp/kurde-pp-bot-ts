
export class TaskManager {
	tasks: { timestamp: number, callback: Function, args: any, repeat: number }[];
	constructor() {
		this.tasks = [];
		this.each_minute()
	}
	each_minute = () => {
		let now = new Date();
		this.tasks.forEach((task, i) => {
			if (task.timestamp - task.timestamp % 60000 == now.getTime() - now.getTime() % 60000) {
				let fun = task.callback;
				let args = task.args;
				if (task.repeat > 0) {
					this.tasks[i].timestamp = task.timestamp + task.repeat;
				}
				else {
					if (this.tasks.length > 1) {
						this.tasks[i] = this.tasks[this.tasks.length - 1]
					}
					this.tasks.pop()
				}
				fun(args);
			}

		})
		now = new Date();
		let delay = 60000 - (now.getTime() % 60000);
		setTimeout(this.each_minute, delay);
	}

}

