import { Client, CommandInteraction, GuildMember, Message, MessageEmbed, User } from "discord.js";
import { DatabaseInterface } from "./database";

export default class StatsManager {
    send_stats = async (interaction: CommandInteraction, DBApp: DatabaseInterface) => {
        const user = interaction.options.getMentionable('user', true)

		if(
			!(user instanceof User) &&
			!(user instanceof GuildMember)
			) return console.error('Wrong mention type.');

		const { id } = user		
        
        let row = await DBApp.get_vc_stats(id)

        let embed = await this.prepare_msg(row ?? { user_id: id, time: 0 }, interaction.client)
	
        interaction.editReply({ embeds: [ embed ] })
    }

    send_statsall = async (interaction: CommandInteraction, DBApp: DatabaseInterface) => {
        let guildId = interaction.guild?.id

        if(!guildId) return interaction.editReply('Error while executing!')

        let row = await DBApp.get_vc_stats_allusers()

        let embed = await this.prepare_msg_all(row, interaction.client)

        interaction.editReply({ embeds: [ embed ] })
    }

    prepare_msg = async (stats: any, client: Client) => {
        let user = await client.users.fetch(stats.user_id)
        let nick = user.username ?? 'brak nicku'
        let image = user.avatarURL() ?? ''

        let embed = new MessageEmbed()
            .setTitle(nick)
            .setDescription(`Czas na kanałach: **${stats.time} minut**`)
            .setColor('BLURPLE')
            .setThumbnail(image)
            .setTimestamp()
        
        return embed
    }

    prepare_msg_all = async (stats: any, client: Client) => {        
        let content = ''

        if(!stats) content = 'Brak statystyk!'
        else {
            for(const stat of stats) {
                let user = await client.users.fetch(stat.user_id)
                let nick = user.username ?? 'wtf'
    
                content += `${nick} - **${stat.time} minut**\n`
            }
        }

        let embed = new MessageEmbed()
            .setTitle('Staty wszystkich')
            .setDescription(content)
            .setColor('BLURPLE')
            .setTimestamp()
        
        return embed
    }
}
